#!/bin/python3

# AI for Mr. X

from engine.player import Player
from engine import boardmap
from typing import List, Tuple, Any
from . import utils
from queue import *
from copy import deepcopy
from collections import deque


# Play Move, takes mr x and returns the move he wishes to make
# Returns: the chosen move, a tuple ((int) new location, (string) transport type)

def play_move(mr_x: Player, detectives: List[Player], x_history: List[Tuple[int, str]]) -> Tuple[int, str]: 
    move = totally_not_just_bfs(mr_x, detectives, x_history, 0, 3)
    print("PREDICTED SCORE: " + str(move[1]))
    return move[0]
    # best_move = None
    # best_score = -1 * float('inf')

    # for transport in boardmap[mr_x.pos]:
    #     for move in boardmap[mr_x.pos][transport]:
    #         x_history_copy = deepcopy(x_history)
    #         # check if in reveal turns
    #         x_history_copy.append(('None', transport))
    #         score = totally_not_just_bfs(mr_x, detectives, x_history_copy, 1, 3)
    #         print("Move", move, "score", score)
    #         if score > best_score:
    #             best_score = score
    #             best_move = (move, transport)
    # return best_move



def totally_not_just_bfs(mr_x, detectives, x_history, turn, depth) -> Tuple[Any, int]:
    if depth > 0:
        if turn == 0:
            best_move = None
            best_score = -1 * float('inf')
            for transport in boardmap[mr_x.pos]:
                for move in boardmap[mr_x.pos][transport]:
                    x_copy = deepcopy(mr_x)
                    x_copy.pos = move
                    x_history_copy = deepcopy(x_history)
                    # if is reveal turn not just none
                    if len(x_history_copy) - 1 in [3, 8, 13, 24]:
                        x_history_copy.append((move, transport))
                    else:
                        x_history_copy.append(('None', transport))
                    score = totally_not_just_bfs(x_copy, detectives, x_history_copy, turn + 1, depth - 1)[1]



                    #print("      "*(4-depth), "Move", move, "score", score, "depth", depth)
                    if score > best_score:
                        best_score = score
                        best_move = (move, transport)
            return best_move, best_score + utils.get_score(mr_x, detectives, x_history)
        else:
            temp_detectives = deepcopy(detectives)
            for detect in temp_detectives:
                detective_move(mr_x, detect, depth)
            return None, totally_not_just_bfs(mr_x, temp_detectives, x_history, 0, depth)[1]
    else:
        return None, utils.get_score(mr_x, detectives, x_history)


def detective_move(mr_x, detective, depth):
    if detective.pos == mr_x.pos:
        return 0

    moves = []
    visited = {detective.pos}
    for t in detective.tickets:
        tickets = detective.tickets.copy()
        tickets[t] -= 1
        if detective.tickets[t] > 0 and t in boardmap[detective.pos]:
            for move in boardmap[detective.pos][t]:
                moves.append((move, tickets, (move, t)))
                visited.add(move)

    dq = deque(moves)
    target = mr_x.pos
    while len(dq) > 0:
        d = dq.pop()
        if d[0] == target:
            detective.pos = d[2][0]
            #print("      "*(4-depth), detective.name, "move to",detective.pos)
            # what happens if he doesnt have enough tickets?
            detective.tickets[d[2][1]] -= 1
            return

        for k in d[1]:
            if d[1][k] > 0:
                tickets = d[1].copy()
                tickets[k] -= 1
                if k not in boardmap[d[0]]:
                    continue
                for loc in boardmap[d[0]][k]:
                    if loc not in visited:
                        dq.appendleft((loc, tickets, d[2]))
                        visited.add(loc)

x = Player({"taxi": 10, "underground": 5}, 194, "x")
x_history = [(None, "taxi")]
a = Player({"taxi": 10, "underground": 8, "bus": 8}, 130, "d")
b = Player({"taxi": 10, "underground": 8, "bus": 8}, 165, "d")
c = Player({"taxi": 10, "underground": 8, "bus": 8}, 71, "d")
d = Player({"taxi": 10, "underground": 8, "bus": 8}, 153, "d")
e = Player({"taxi": 10, "underground": 8, "bus": 8}, 149, "d")

#print(play_move(x, [a, b, c, d, e], x_history))
