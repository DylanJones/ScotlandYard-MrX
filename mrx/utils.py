#!/bin/python

from engine import boardmap
from engine.player import Player
from collections import deque
from queue import Queue


# adjacency matrix
adj_all = {} # all adjancencies, including ferries
adj_normal = {} # only cop-traversable adjancies
for node in boardmap:
    adj_normal[node] = set()
    adj_all[node] = set()
    for transport in boardmap[node]:
        adj_all[node].update(set(boardmap[node][transport]))
        if transport != "black":
            adj_normal[node].update(set(boardmap[node][transport]))

shortest_paths = {}

# creates distance dictionary [node 1, node 2, node 3, etc.]
# where each dictonary stores kv-pair node: shortest distance to node
for i in adj_normal:
    this_dict = {i: 0}

    current = Queue()
    current.put(i)

    while not current.qsize() == 0:
        num = current.get()
        for x in adj_all[num]: ############ ALL OR NORMAL?
            if x not in this_dict:
                this_dict[x] = this_dict[num] + 1
                current.put(x)


#TODO BORK BORK BORK    shortest_paths.append(this_dict)

# count all possible locations x can be based on history
def calc_ambiguity(x_history):
    start_index = len(x_history) - 1
    if start_index < 3:
        return 4
    while start_index > 0 and x_history[start_index][0] is None:
        start_index -= 1
    possible_nodes = [x_history[start_index][0]]
    start_index += 1
    while start_index < len(x_history):
        possible_nodes2 = []
        for node in possible_nodes:
            possible_nodes2 += boardmap[node][x_history[start_index][1]]
        possible_nodes = list(set(possible_nodes2))
        start_index += 1
    return len(possible_nodes)


# for testing
x = Player({"taxi": 10, "underground": 5}, 62, "x")
d = Player({"taxi": 10, "underground": 8, "bus": 8}, 61, "d")

# returns the shortest path a detective can take to get mr x, taking into account tickets
# REALLY slow, should improve
def detective_min_distance(mr_x, detective):
    if detective.pos == mr_x.pos:
        return 0

    moves = []
    visited = {detective.pos}
    for t in detective.tickets:
        tickets = detective.tickets.copy()
        tickets[t] -= 1
        if detective.tickets[t] > 0 and t in boardmap[detective.pos]:
            for move in boardmap[detective.pos][t]:
                moves.append((move, tickets, 1))
                visited.add(move)

    dq = deque(moves)
    target = mr_x.pos
    while len(dq) > 0:
        d = dq.pop()
        if d[0] == target:
            return d[2]

        for k in d[1]:
            if d[1][k] > 0:
                tickets = d[1].copy()
                tickets[k] -= 1
                if k not in boardmap[d[0]]:
                    continue
                for loc in boardmap[d[0]][k]:
                    if loc not in visited:
                        dq.appendleft((loc, tickets, d[2]+1))
                        visited.add(loc)
    return 10



# for testing

def get_score(mr_x, detectives, x_history):
    dists = [detective_min_distance(mr_x, d) for d in detectives]
    modifier = 0
    for x in dists:
        if x == 1:
            modifier += -100000 # about to be caught
        if x == 0:
            modifier += -100000000 # already caught
    dists.sort()
    return dists[0]*2+dists[1]*1.5+dists[2]*1.25+dists[3]+dists[4]+modifier+calc_ambiguity(x_history)

